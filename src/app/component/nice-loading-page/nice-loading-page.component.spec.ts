import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NiceLoadingPageComponent } from './nice-loading-page.component';

describe('NiceLoadingPageComponent', () => {
  let component: NiceLoadingPageComponent;
  let fixture: ComponentFixture<NiceLoadingPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NiceLoadingPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NiceLoadingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
