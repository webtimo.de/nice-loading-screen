import {Component, Input, OnDestroy, OnInit} from '@angular/core';

@Component({
    selector: 'app-nice-loading-page',
    templateUrl: './nice-loading-page.component.html',
    styleUrls: ['./nice-loading-page.component.css']
})
export class NiceLoadingPageComponent implements OnInit, OnDestroy {


    @Input() percent: number = 0;
    @Input() backgroundImage: string = "";
    @Input() doText: string = "";

    public movingDots: string = "...";
    public movingDotsHTML: string = "...";
    private readonly movingDotsTimer: any;

    constructor() {

        this.movingDotsTimer = setInterval(() => {
            if (this.movingDots.length === 0) {
                this.movingDots = ".";
                this.movingDotsHTML = ".  ";
            } else if (this.movingDots.length === 1) {
                this.movingDots = "..";
                this.movingDotsHTML = ".. ";
            } else if (this.movingDots.length === 2) {
                this.movingDots = "...";
                this.movingDotsHTML = "...";
            } else if (this.movingDots.length === 3) {
                this.movingDots = "";
                this.movingDotsHTML = "   ";
            }
        }, 500);

    }


    public getDoText(): string {
        return this.doText ? this.doText + this.movingDots : '';
    }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
        clearInterval(this.movingDotsTimer);
    }

}
