import {Component} from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    public title = 'nice-loading-screen';
    public percent: number = 0;


    constructor() {
        setTimeout(() => {
            this.startDummy();
        }, 2000);
    }


    public startDummy(): void {
        this.percent = 0;
        const interval = setInterval(() => {
            this.percent++;

            if (this.percent >= 100) {
                clearInterval(interval);
            }
        }, 250)
    }

}
