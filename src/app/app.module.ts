import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NiceLoadingPageComponent } from './component/nice-loading-page/nice-loading-page.component';
import {NgCircleProgressModule} from "ng-circle-progress";

@NgModule({
  declarations: [
    AppComponent,
    NiceLoadingPageComponent
  ],
    imports: [
        BrowserModule,
        NgCircleProgressModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
